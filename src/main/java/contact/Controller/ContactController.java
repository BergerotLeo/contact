package contact.Controller;

import contact.Entity.Contact;
import contact.Repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class ContactController {

    @Autowired
    protected ContactRepository contactRepository;

    /**
     * / return to /contacts
     *
     * @return
     */
    @GetMapping("/")
    public String redirectToContactList() {
        return "contacts";
    }

    /**
     * contact List
     *
     * @param model
     * @return
     */
    @GetMapping("/contacts")
    public String getContacts(Model model) {
        Iterable<Contact> contactList = this.contactRepository.findAll();
        model.addAttribute("contacts", contactList);
        return "contacts";
    }

    /**
     * show contact form for edit contact
     *
     * @param id
     * @param model
     * @return
     */
    @GetMapping("/edit_contact")
    public String editContact(@RequestParam long id, Model model) {
        Optional<Contact> optionalContact = this.contactRepository.findById(id);
        if (optionalContact.isPresent()) {
            model.addAttribute("contact", optionalContact.get());
            return "contactForm";
        }

        return "redirect:/contacts";
    }

    /**
     * show contact form for create contact
     *
     * @param contact
     * @return
     */
    @GetMapping("/add_contact")
    public String showForm(Contact contact) {
        return "contactForm";
    }

    /**
     * create contact
     *
     * @param contact
     * @param bindingResult
     * @return
     */
    @PostMapping("/contacts")
    public String createContact(@Valid Contact contact, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "contactForm";
        }

        this.contactRepository.save(contact);

        return "redirect:/contacts";
    }

    /**
     * update contact
     *
     * @param contact
     * @param bindingResult
     * @return
     */
    @PutMapping("/contacts")
    public String editContact(@Valid Contact contact, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "contactForm";
        }

        this.contactRepository.save(contact);

        return "redirect:/contacts";
    }

    /**
     * delete contact
     *
     * @param id
     * @param model
     * @return
     */
    @DeleteMapping("/delete_contact")
    public String deleteContact(@RequestParam long id, Model model) {
        Optional<Contact> optionalContact = this.contactRepository.findById(id);
        optionalContact.ifPresent(contact -> this.contactRepository.delete(contact));

        return "redirect:/contacts";
    }
}
