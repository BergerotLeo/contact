package contact.Controller;

import contact.Entity.Contact;
import contact.Entity.Contacts;
import contact.Repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;


@RestController
public class APIContactController {

    @Autowired
    protected ContactRepository contactRepository;

    @GetMapping(value = "/api/contacts/{id}", produces = {MediaType.APPLICATION_XML_VALUE})
    public Contact contactXML(@PathVariable long id) {
        Contact contact = null;
        Optional<Contact> optionalContact = this.contactRepository.findById(id);
        if (optionalContact.isPresent()) {
            contact = optionalContact.get();
        }

        return contact;
    }

    @GetMapping(value = "/api/contacts", produces = {MediaType.APPLICATION_XML_VALUE})
    public Contacts contactXML() {
        Contacts contacts = new Contacts();
        contacts.setContacts((List<Contact>) this.contactRepository.findAll());
        return contacts;
    }

}
